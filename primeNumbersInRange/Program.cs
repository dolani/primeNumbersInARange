﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace primeNumbersInRange
{
    class Program
    {
        static void Main(string[] args)
        {
            //using stopwatch to get the time elapsed
            //instantiate and start the watch
            var watch = Stopwatch.StartNew();

            int lowerBound, upperBound;
            //This program prints the prime numbers between two given numbers
            //Prompt user to input lower bound number
            Console.WriteLine("Enter the first number");
            lowerBound = Convert.ToInt32(Console.ReadLine());

            //Prompt user to input upper bound number
            Console.WriteLine("Enter the second number");
            upperBound = Convert.ToInt32(Console.ReadLine());

            //check if the user inputs a firstNumber greater than the secondNumber
            if (lowerBound > upperBound){
                Console.WriteLine("Error, your first number must be lower than your second number");
                Console.ReadKey();
                return;
            };

            for (int i = lowerBound; i <= upperBound; i++)
            {
                int c = 0;
                for (int j = 1; j <= upperBound; j++)
                {
                    if (i%j == 0){
                        c++;
                    };
                };
                if (c == 2)
                {
                    Console.Write(i + "\t");
                }
            }
            //stop the watch
            watch.Stop();

            //check elapsed time in seconds
            Console.WriteLine("Time taken {0} sec!", (watch.ElapsedTicks/Stopwatch.Frequency));
            
            Console.ReadKey();
        }
    }
}
